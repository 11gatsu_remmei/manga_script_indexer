/* Copyright 2018 11gatsu_remmei
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org._11gatsu_remmei.manga_script_indexer

import play.api.libs.json._

/** 発話1つ。
  * @param bookTitle 書籍名
  * @param volume 巻数名
  * @param volumeSortKey 巻数でソートするときの値
  * @param page ページ
  * @param region ページの中の領域(右、左、上など)
  * @param panel 領域中でのコマの位置
  * @param speaker 話者
  * @param words 発話内容
  */
case class Speech(bookTitle: String,
                  volume: String,
                  volumeSortKey: Int,
                  page: Int,
                  region: Option[String],
                  panel: Int,
                  speaker: String,
                  words: String,
                  rubyAnnotations: Seq[(String, String)] = Seq.empty) {
  /** コマの位置を表す文字列の列を返す。 */
  def location: Seq[String] = {
    val pageString = if (page < 0) {
      Speech.coverPageNames.getOrElse(page, {
                                        println(s"WARN: invalid page ${page}")
                                        ""
                                      })
    } else {
      s"p. ${page}"
    }

    Seq(
      volume,
      pageString,
      region.getOrElse(""),
      if (panel == 0) "" else (panel.toString + "コマ目")
    )
  }

  /** JSONオブジェクトにして返す。 */
  def toJson: JsObject = Json.obj(
    "book" -> bookTitle,
    "volume" -> volume,
    "volumeSortKey" -> volumeSortKey,
    "page" -> page,
    "region" -> region.getOrElse[String](""),
    "panel" -> panel,
    "speaker" -> speaker,
    "words" -> words
  ) ++ (
    if (rubyAnnotations.isEmpty) {
      JsObject.empty
    } else {
      Json.obj(
        "rubyAnnotations" -> rubyAnnotations
      )
    }
  )

  /** JSON文字列にして返す。 */
  def toJsonString: String = toJson.toString

  /** ルビを追加したオブジェクトのコピーを返す。
    *
    * @param base ルビが振られた元の文字列
    * @param annotation ルビの文字列
    */
  def rubyAppended(base: String, annotation: String): Speech = Speech(
    bookTitle,
    volume,
    volumeSortKey,
    page,
    region,
    panel,
    speaker,
    words,
    rubyAnnotations :+ ((base, annotation))
  )
}

object Speech {
  val coverPageNames = Map(
    -99 -> "カバー表1",
    -98 -> "カバー表2",
    -97 -> "カバー表3",
    -96 -> "カバー表4",
    -95 -> "カバー背表紙",
    -94 -> "カバー下表1",
    -93 -> "カバー下表2",
    -92 -> "カバー下表3",
    -91 -> "カバー下表4",
    -90 -> "カバー下背表紙"
  )
}
