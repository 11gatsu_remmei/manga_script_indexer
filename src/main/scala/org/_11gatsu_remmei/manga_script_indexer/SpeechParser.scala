/* Copyright 2018 11gatsu_remmei
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org._11gatsu_remmei.manga_script_indexer

import scala.io.Source
import scala.collection.mutable

class SpeechParser {
  import SpeechParser._

  /** ファイルを読み込んで発話の列を返す。 */
  def parseText(filename: String): Seq[Speech] = {
    val source = Source.fromFile(filename, "UTF-8")

    var bookTitle = ""
    var volume = ""
    var volumeSortKey = 0
    var currentPage = 0
    var currentRegion = None: Option[String]
    var currentPanel = 0
    var speeches = mutable.Buffer.empty[Speech]

    for ((line, lineIndex) <- source.getLines().zipWithIndex) {
      val lineNumber = lineIndex + 1

      line match {
        case emptyPattern() => ()
        case commentPattern() => ()
        case bookTitlePattern(title) => bookTitle = title
        case volumePattern(vol) => volume = vol
        case volumeSortKeyPattern(key) => volumeSortKey = key.toInt
        case pagePattern(page) => {
          currentPage = page.toInt
          currentRegion = None
          currentPanel = 0
        }
        case coverPagePattern(page) => {
          currentPage = 5 + page.toInt  - 100
          currentRegion = None
          currentPanel = 0
        }
        case spinePattern() => {
          currentPage = 10 - 100
          currentRegion = None
          currentPanel = 0
        }
        case jacketPagePattern(page) => {
          currentPage = page.toInt - 100
          currentRegion = None
          currentPanel = 0
        }
        case jacketSpinePattern() => {
          currentPage = 5 - 100
          currentRegion = None
          currentPanel = 0
        }
        case rightPattern() => {
          currentRegion = Some("右")
          currentPanel = 1
        }
        case leftPattern() => {
          currentRegion = Some("左")
          currentPanel = 1
        }
        case upperPattern() => {
          currentRegion = Some("上")
          currentPanel = 0
        }
        case lowerPattern() => {
          currentRegion = Some("下")
          currentPanel = 0
        }
        case panelSeparatorPattern() => {
          currentPanel += 1
        }
        case rubyPattern(base, annotation) =>
          speeches(speeches.length - 1) =
            speeches.last.rubyAppended(base, annotation)
        case speechPattern(speaker, words) => {
          val panel = if (speaker == "タイトル") 0 else currentPanel

          speeches += Speech(
            bookTitle,
            volume,
            volumeSortKey,
            currentPage,
            currentRegion,
            panel,
            speaker,
            words
          )
        }
        case silencePattern() => {
          speeches += Speech(
            bookTitle,
            volume,
            volumeSortKey,
            currentPage,
            currentRegion,
            currentPanel,
            "無言",
            ""
          )
        }
        case _ => {
          println(s"WARN: Invalid line at ${lineNumber}")
        }
      }
    }

    speeches.toSeq
  }
}

object SpeechParser {
  private val commentPattern = ("^\\s*#.*$").r
  private val emptyPattern = ("^\\s*$").r
  private val bookTitlePattern = ("^書籍名 (.*)$").r
  private val volumePattern = ("^巻数 (.*)$").r
  private val volumeSortKeyPattern = ("^巻数ソートキー (.*)$").r
  private val pagePattern = ("^p([0-9]+)$").r
  private val coverPagePattern = ("^表([0-9]+)$").r
  private val spinePattern = ("^背表紙$").r
  private val jacketPagePattern = ("^カバー表([0-9]+)$").r
  private val jacketSpinePattern = ("^カバー背表紙$").r
  private val rightPattern = "^右$".r
  private val leftPattern = "^左$".r
  private val upperPattern = "^上$".r
  private val lowerPattern = "^下$".r
  private val panelSeparatorPattern = "^-$".r
  private val rubyPattern = "^ルビ (.*) (.*)$".r
  private val speechPattern = "^([^ ]+) (.*)$".r
  private val silencePattern = ("^無言$").r
}
