/* Copyright 2018 11gatsu_remmei
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org._11gatsu_remmei.manga_script_indexer

import scala.collection.mutable

class Indexer {
  /** 同じコマの同じ人物のセリフを統合して返す。 */
  private def fuseSpeechesBySameSpeaker(
    speeches: Iterable[Speech]
  ): Iterable[Speech] =
    speeches
      .groupBy {
        speech => (
          speech.bookTitle,
          speech.volume,
          speech.volumeSortKey,
          speech.page,
          speech.region,
          speech.panel,
          speech.speaker
        )
      }
      .map {
        case (
          (bookTitle, volume, volumeSortKey, page, region, panel, speaker),
          group
        ) =>
          Speech(
            bookTitle,
            volume,
            volumeSortKey,
            page,
            region,
            panel,
            speaker,
            group.map(_.words).mkString(" "),
            group.flatMap(_.rubyAnnotations).toSeq
          )
      }

  /** 句読点等を削除した文字列を返す。 */
  private def stripPunctuations(words: String): String =
    // TODO Unicodeのカテゴリを使う?
    words
      .replaceAll("[-、。,.!?・()「」『』\"'“”〜ー—…]", "")
      .replaceAll("([^\\x00-\\x7F]) ", "$1")
      .replaceAll(" ([^\\x00-\\x7F])", "$1")
      .toLowerCase

  private def codePointCount(string: String) =
    string.codePointCount(0, string.length)

  /** ルビ付きテキストをグラフ(DAG)で表現して返す。
    *
    * グラフは、ルビが付くテキストの開始時点で、元のテキストとルビに分岐する。
    * 元のテキストとルビが終わったところで合流する。
    *
    * ノードのIDは、ルビ対象文字列の後にルビを挿入した文字列内における
    * 部分文字列のインデックスである。
    *
    * @param words 発話
    * @param rubyAnnotations ルビの定義の列。
    *                        ベースとなる単語とよみがなのペアの列
    */
  private def expandRubyAnnotations(
    words: String,
    rubyAnnotations: Seq[(String, String)]
  ): Iterable[GraphNode[String]] = {
    var lastIndex = 0
    val chunks = mutable.Buffer.empty[Iterable[String]]

    rubyAnnotations.foreach {
      case (base, annotation) => {
        val index = if (lastIndex < words.length) {
          words.indexOf(base, lastIndex)
        } else {
          -1
        }

        if (index == -1) {
          throw new RuntimeException(s"invalid ruby ${base}, ${annotation}, ${words}")
        }

        if (lastIndex != index) {
          chunks += Iterable(words.substring(lastIndex, index))
        }

        chunks += Iterable(base, annotation)

        lastIndex = index + base.length
      }
    }

    if (lastIndex != words.length) {
      chunks += Iterable(words.substring(lastIndex))
    }

    def toGraph(
      chunks: Iterator[Iterable[String]],
      position: Int
    ): Iterable[GraphNode[String]] = {
      if (!chunks.hasNext) {
        return Iterable.empty
      }

      val chunk = chunks.next()
      val nextPosition =
        position + chunk.map(codePointCount _).sum

      val followingGraph = toGraph(chunks, nextPosition)

      val positions = chunk.scanLeft(position)(
        (position, text) => position + codePointCount(text)
      )

      chunk.zip(positions).map {
        case (text, position) => GraphNode(position, text, followingGraph)
      }
    }

    toGraph(chunks.iterator, 0)
  }

  /** n-gramをノードとして持つグラフを返す。
    *
    * ノードの値はn-gramのテキストである。
    *
    * @param words 発話
    * @param rubyAnnotations ルビの定義の列。
    *                        ベースとなる単語とよみがなのペアの列
    * @param size n-gramのn
    */
  private def generateNGrams(
    words: String,
    rubyAnnotations: Seq[(String, String)],
    size: Int
  ): Iterable[GraphNode[String]] = {
    val cache = mutable.Map.empty[Int, mutable.Buffer[GraphNode[String]]]

    def generate(
      graph: Iterable[GraphNode[String]]
    ): Iterable[GraphNode[String]] =
      graph.flatMap(node => cache.getOrElse(node.id, doGenerate(node)))

    def substring(string: String, beginOffset: Int, endOffset: Int) =
      string.substring(string.offsetByCodePoints(0, beginOffset),
                       string.offsetByCodePoints(0, endOffset))

    def doGenerate(node: GraphNode[String]): Iterable[GraphNode[String]] = {
      val text = stripPunctuations(node.value)
      val followingGraph = generate(node.nexts)

      for (index <- 0.until(codePointCount(text)).reverse) {
        val position = node.id + index
        val nexts = if (index == codePointCount(text) - 1) {
          followingGraph
        } else {
          cache.getOrElseUpdate(position + 1, mutable.Buffer.empty)
        }

        def emit(ngram: String): Unit = {
          val nextsFiltered = nexts
            .filter(n => codePointCount(n.value) == size)
            .filter(_.value.startsWith(ngram.substring(1)))

          cache.getOrElseUpdate(position, mutable.Buffer.empty) +=
            GraphNode(position, ngram, nextsFiltered)
        }

        if (index + size <= codePointCount(text)) {
          emit(substring(text, index, index + size))
        } else {
          val followingTexts = if (followingGraph.isEmpty) {
            Seq("")
          } else {
            followingGraph.map(_.value)
          }

          for (next <- followingTexts) {
            val appended = text + next
            val ngram = substring(
              appended,
              index,
              math.min(index + size, codePointCount(appended))
            )

            emit(ngram)
          }
        }
      }

      cache.getOrElseUpdate(node.id, mutable.Buffer.empty)
    }

    generate(expandRubyAnnotations(words, rubyAnnotations))

    cache.values.flatten.filter {
      node => codePointCount(node.value) == size
    }
  }

  /** インデックスから除外すべきunigramであればtrueを返す。
    *
    * インデックスから原文を復元しにくくするため、検索されなさそうな
    * unigramを除外する。
    */
  private def isStopUnigram(unigram: String): Boolean =
    // ひらがなとカタカナ
    // TODO 他の基本的な漢字も含める?
    unigram.matches("[ ぁ-ゟ゠-ヿ]")

  /** 発話の列からインデックスを作って返す。 */
  def makeIndex(speeches: Iterable[Speech]) = {
    val fused = fuseSpeechesBySameSpeaker(speeches)
    val index = new Index

    for (speech <- fused) {
      val words = speech.words

      def addAll(ngrams: Iterable[GraphNode[String]]): Unit = {
        for (node <- ngrams) {
          index.add(
            node.value,
            node.id,
            node.nexts.map(_.id).toSet.toSeq.sorted,
            speech
          )
        }
      }

      addAll(generateNGrams(words, speech.rubyAnnotations, 1)
               .filterNot(node => isStopUnigram(node.value)))

      addAll(generateNGrams(words, speech.rubyAnnotations, 2))
    }

    index
  }
}
