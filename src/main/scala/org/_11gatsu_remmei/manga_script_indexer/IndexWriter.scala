/* Copyright 2018 11gatsu_remmei
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org._11gatsu_remmei.manga_script_indexer

import play.api.libs.json._

import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Path
import java.util.Base64
import java.util.Collections

class IndexWriter {
  def saveIndex(baseDirectory: Path, index: Index): Unit = {
    baseDirectory.toFile.mkdirs()

    val base64Encoder = Base64.getUrlEncoder.withoutPadding

    for ((word, entries) <- index.toMap) {
      val bytes = word.getBytes(StandardCharsets.UTF_8)
      val filename = base64Encoder.encodeToString(bytes)
      // val filename = bytes.map(byte => "%02x".format(byte)).mkString("")

      val jsonString = JsArray(
        entries.toSeq.map { entry =>
          Json.obj(
            // "word" -> word,
            "book" -> entry.book,
            "sortKey" -> entry.sortKey,
            "location" -> entry.location,
            "position" -> entry.position,
            "speaker" -> entry.speaker
          ) ++ (
            if (!entry.nexts.isEmpty &&
                  !(entry.nexts.size == 1 &&
                      entry.nexts.iterator.next() == entry.position + 1)) {
              Json.obj("nexts" -> entry.nexts)
            } else {
              JsObject.empty
            }
          )
        }
      ).toString

      Files.write(
        baseDirectory.resolve(filename + ".json"),
        Collections.singleton(jsonString),
        StandardCharsets.UTF_8
      )
    }
  }
}
