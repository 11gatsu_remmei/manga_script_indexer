/* Copyright 2018 11gatsu_remmei
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org._11gatsu_remmei.manga_script_indexer

import scala.collection.mutable

/** n-gramのインデックス */
class Index {
  /** インデックスの本体 */
  private val map = mutable.Map.empty[String, mutable.Buffer[IndexEntry]]

  /** インデックスに項目を追加する。
    *
    * @param word n-gram1つ
    * @param position 領域中でのコマの位置
    * @param nexts 次のn-gramのpositionの配列。
    *              省略時は「position + 1」1つだけからなる配列である。
    */
  def add(word: String, position: Int, nexts: Iterable[Int], speech: Speech)
      : Unit = {
    def regionToSortKey(region: Option[String]) = region match {
      case None => 0
      case Some("上") => 1
      case Some("右") => 2
      case Some("左") => 3
      case Some("下") => 1
      case _ => 9
    }

    val sortKey = Seq(
      speech.volumeSortKey,
      speech.page,
      regionToSortKey(speech.region),
      speech.panel
    )

    val entry = IndexEntry(
      speech.bookTitle,
      speech.location,
      sortKey,
      speech.speaker,
      position,
      nexts
    )

    map.getOrElseUpdate(word, mutable.Buffer.empty[IndexEntry]) += entry
  }

  /** インデックスをMapにして返す。 */
  def toMap: Map[String, Iterable[IndexEntry]] =
    map.view.mapValues(values => Array.empty.toSeq ++ values).toMap
}

/** インデックスの項目1つ。n-gramの出現1つに相当する。
  *
  * ルビがあるため、「次のn-gram」は1つではなく、複数ある。
  *
  * @param book 書籍名
  * @param location コマの位置を表す表示用の文字列。階層化されていて、最初の要素が最も荒い。
  * @param sortKey コマの位置を表すソート用の数値の列。
  * @param speaker 話者
  * @param position コマの中の同一話者のセリフの中での位置
  * @param nexts 次のn-gramのpositionの配列。
  */
case class IndexEntry(
  book: String,
  location: Seq[String],
  sortKey: Seq[Int],
  speaker: String,
  position: Int,
  nexts: Iterable[Int]
) {
}
