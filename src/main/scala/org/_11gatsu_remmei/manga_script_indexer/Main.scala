/* Copyright 2018 11gatsu_remmei
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org._11gatsu_remmei.manga_script_indexer

import java.nio.file.Paths

object Main {
  /** 発話者の一覧を表示する。 */
  private def showSpeakers(speeches: Iterable[Speech]): Unit = {
    println("Speakers:")
    println(speeches
              .map(_.speaker)
              .toSet
              .toSeq
              .sorted
              .map("  " + _)
              .mkString("\n"))
  }

  /** 領域あたりのコマ数のチェックをして結果を表示する。 */
  private def lintPanels(speeches: Iterable[Speech]): Unit = {
    val byPages = speeches.groupBy { speech =>
      (speech.bookTitle, speech.volume, speech.page)
    }
    val byRegion = speeches.groupBy { speech =>
      (speech.bookTitle, speech.volume, speech.page, speech.region)
    }

    for {
      (
        (bookTitle, volume, page, region),
        speeches
      ) <- byRegion.toSeq.sortBy(_._1)
    } {
      val maxPanel = speeches.map(_.panel).max
      val location = s"${bookTitle} ${volume} p.${page} ${region.getOrElse("")}"

      if (maxPanel == 3) {
        println(s"WARN: only 3 panels: ${location}")
      } else if (maxPanel == 1 &&
                   (region == Some("右") || region == Some("左"))) {
        println(s"WARN: only 1 panels: ${location}")
      } else if (maxPanel == 2 &&
                   (region == Some("右") || region == Some("左"))) {
        // トビラページは除く

        val speechesOnSamePage = byPages
          .getOrElse((bookTitle, volume, page), Seq.empty)

        val hasUpperOrLower = speechesOnSamePage
          .exists { speech =>
            speech.region == Some("上") || speech.region == Some("下") ||
            speech.region == None
          } || {
            val rightPanels = speechesOnSamePage.filter { speech =>
              speech.region == Some("右")
            }
            val leftPanels = speechesOnSamePage.filter { speech =>
              speech.region == Some("左")
            }

            (!rightPanels.isEmpty && !leftPanels.isEmpty &&
               rightPanels.map(_.panel).max == 2 &&
               leftPanels.map(_.panel).max == 2)
          }

        if (!hasUpperOrLower) {
          println(s"WARN: only 2 panels: ${location}")
        }
      }
    }
  }

  def main(args: Array[String]): Unit = {
    val speechParser = new SpeechParser()
    val speeches = args.flatMap(speechParser.parseText _)

    val indexer = new Indexer()
    val index = indexer.makeIndex(speeches)

    showSpeakers(speeches)
    lintPanels(speeches)

    // println(speeches.toIterator.map(_.toJsonString).mkString("[\n", ",\n", "\n]"))
    // println(index.map.keys.toArray.filter(_.length == 1).sorted.mkString(""))
    // println(index.map.keys.toArray.filter(_.length == 2).sorted.mkString(" "))

    val indexWriter = new IndexWriter()

    indexWriter.saveIndex(Paths.get("index"), index)
  }
}
