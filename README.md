# 必殺まぞくさがし インデクサ

このプログラムは漫画台詞全文検索システム「必殺まぞくさがし」用のインデックスを生成するものです。

台詞等を書いたテキストファイルを引数として渡します。
形式は`sample`フォルダ内のファイルを見てください。
複数のファイルに分けた場合は全てを引数として指定する必要があります。

実行例:

```
sbt stage && ./target/universal/stage/bin/manga_script_indexer text1 text2
```

インデックスは`index`というフォルダに作られます。
これを「必殺まぞくさがし」の`dist`ディレクトリに置いてください。

```
mkdir -p ../machi_search/dist && cp -a index ../machi_search/dist/
```
