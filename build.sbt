enablePlugins(JavaAppPackaging)

organization := "org._11gatsu_remmei"
name := "manga_script_indexer"
version := "1.0"
scalaVersion := "2.13.7"

scalacOptions ++= Seq(
  "-feature",
  "-unchecked",
  "-deprecation",
  "-Xcheckinit",
  "-Xfatal-warnings",
  "-encoding", "UTF-8"
)

libraryDependencies ++= Seq(
  "com.typesafe.play" %% "play-json" % "2.9.2"
)
